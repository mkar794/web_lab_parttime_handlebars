var express = require('express');
var fs = require('fs');

var app = express();

app.set("port", process.env.PORT || 3000 );

var handlebars = require('express-handlebars');
app.engine("handlebars", handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(express.static(__dirname + "/public"));

// This code will redirect all the URLs that end with a "/" to the same URL but without the "/".
// For example, if the user navigates to http://localhost:3000/about/, they will be redirected
// to http://localhost:3000/about. This will make the image URLs and stuff in the HTML files always
// work correctly.

// app.use(function(req, res, next) {
//     if(req.path.substr(-1) == "/" && req.path.length > 1) {
//         var query = req.url.slice(req.path.length);
//         res.redirect(301, req.path.slice(0, -1) + query);
//     } else {
//         next();
//     }
// });

app.get("/", function(req, res) {

    var hobbies = [
        "sleeping", "Reading", "Procrastinating"
    ];


   // var randomIndex = Math.floor(Math.random() * hobbies.length);

    var context = {
        //hobby: null,
        hobby: hobbies,
        //randomHobbies: hobbies[randomIndex]
    };

    //console.log(hobbies[randomIndex]);
    //console.log("random: " + randomHobby);
    res.render('about', context);
});

// 404 error page route
app.use(function(req, res, next) {
    res.type('text/html');
    res.status(404);
    // res.sendFile(__dirname + "/public/404-error.html");
    res.render('404');
})
// 500 error page route
app.use(function(err, req, res, next) {
    res.type('text/html');
    console.log(err.stack);
    res.status(500);
    res.render('500');
});

app.listen( app.get('port'), function(){
    console.log("Express started on http://localhost:" + app.get('port'));
});